//npx javascript-obfuscator --unicode-escape-sequence true index.js
globalThis.document.querySelector('div[style*="flex-direction"] > button').onclick = () =>
{
    globalThis.document.querySelector('div[style*="flex-direction"]').style.width = globalThis.document.querySelector('body > nav').style.marginLeft = globalThis.document.querySelector('div[style*="justify-content"]').style.marginLeft = globalThis.document.querySelector('video').style.marginLeft = 0
}
for (const button of globalThis.document.querySelectorAll('div[style*="justify-content"] button')) button.onclick = async event =>
{
    const list = globalThis.document.querySelector('div[style*="flex-direction"]') 
    for (const hyperlink of list.querySelectorAll('a')) hyperlink.remove()
    for (const episode of await globalThis.fetch('https://backend.chaowenguo.eu.org/list', {method:'post', headers:{'content-type':'application/x-www-form-urlencoded'}, body:event.target.textContent}).then(_ => _.json()))
    {
        const hyperlink = globalThis.document.createElement('a')
	hyperlink.href = ['', event.target.textContent, episode].join('/')
        hyperlink.textContent = episode
        list.appendChild(hyperlink)
    }
    globalThis.document.querySelector('div[style*="flex-direction"]').style.width = globalThis.document.querySelector('body > nav').style.marginLeft = globalThis.document.querySelector('div[style*="justify-content"]').style.marginLeft = globalThis.document.querySelector('video').style.marginLeft = '10%'
}
const drama = globalThis.decodeURIComponent(globalThis.location.pathname).slice(1)
globalThis.document.querySelector('title').textContent = globalThis.document.querySelector('h1').textContent = drama.split('/').at(-1)
if (drama)
{
    for (const episode of globalThis.Array(globalThis.Number(await globalThis.fetch('https://backend.chaowenguo.eu.org/episode', {method:'post', headers:{'content-type':'application/x-www-form-urlencoded'}, body:drama}).then(_ => _.text()))).keys())
    {
        const button = globalThis.document.createElement('button')
        button.textContent = (episode + 1).toString().padStart(2, 0)
        globalThis.document.querySelector('body > nav').appendChild(button)
    }
    for (const button of globalThis.document.querySelectorAll('body > nav > button')) button.onclick = async event =>
    {
        globalThis.document.querySelector('video').querySelector('source').src = await globalThis.fetch('https://backend.chaowenguo.eu.org/mp4', {method:'post', headers:{'content-type':'application/json'}, body:globalThis.JSON.stringify({drama, episode:event.target.textContent})}).then(_ => _.text())
        globalThis.document.querySelector('video').querySelector('track').src = globalThis.URL.createObjectURL(await globalThis.fetch('https://backend.chaowenguo.eu.org', {method:'post', body:globalThis.JSON.stringify({drama, episode:event.target.textContent})}).then(_ => _.blob()))
        globalThis.document.querySelector('video').load()
        globalThis.document.querySelector('title').textContent = drama + event.target.textContent
    }
}